public interface NumListInterface {

    /**
     * Returns the sum over items in a NumList
     * @return the sum over numbers in a NumList
     */
    public int sum();

    /**
    * Returns the sum over items in a NumList
    * DON'T WRITE THE SAME METHOD AS SUM
    * @return the sum over numbers in a NumList
    */
    public int secondarySum();

    /**
     * Finds the index of number if it exists
     * Searches using linear search
     * @param   number  number we are searching for
     * @return index of number in the NumList
     */
    public int linearSearch(int number);

    /**
     * Finds the index of number if it exists
     * Searches using binary search
     * USE RECURSION
     * @param   number  number we are searching for
     * @return index of number in the NumList
     */
    public int binarySearch(int number);

    /**
     * Adds count random numbers to the list - I recommend 100 to start
     * I recommend using this in your constructor
     * I recommend using MAX_RANDOM_VAULE with your random number generator
     * @param   number  number we are searching for
     * @return index of number in the NumList
     */
    public void addRandomNumbers(int count);
}
