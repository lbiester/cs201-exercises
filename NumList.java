/**
 * NumList is a class to get you started using interfaces and inheritance
 * Here's what you should do:
 * 0) DO NOT change any of the code that is already here. Work with it!
 *    It's intended to help you learn.
 * 1) Make this compile. You will probably need to look at NumListInterface
 *    in order to accomplish this first task.
 * 2) Implement the methods in NumListInterface. The comments there should tell
 *    you what to do. Ask me if you have questions.
 * 3) If you have time, try implementing (in no particular order)
 *    (a) Your own sorting algorithm
 *    (b) A binary search using iteration
 *    (c) Summation methods that are different from the ones you already implemented
 *    (d) An "add in order method", that adds elements in the correct position to
 *        keep your list sorted
 *    (e) a "clone" method that copies all of the items in your list to a new list
 *
 * HINTS:
 * - if you want to sort a List<Integer> list, use Sorter.sort(list)
 * - you may find it very useful to create helper functions in some cases. there
 *   is one function here that I believe you cannot write without a helper function,
 *   or modifying your list
 * - you CAN change the order of your list (you will need to for binary search),
 *   unless you create a new list
 * - you CANNOT change the contents of your list (you may be tempted to for
 *   binary search)
 */
public class NumList extends ArrayList<Integer> implements NumListInterface {

    // max out at 100 so that it is easier to see that your methods are working
    // you can also construct non-random lists for this purpose
    private static int MAX_RANDOM_VALUE = 100;
}
